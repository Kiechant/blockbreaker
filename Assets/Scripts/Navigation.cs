﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace BlockBreaker
{
	public static class Navigation
	{
		public struct ActionArg
		{
			public ActionArg(string name, LevelManager.LevelState toScene, object args)
			{
				this.name = name;
				this.toScene = toScene;
				this.args = args;
			}

			public string name;
			public LevelManager.LevelState toScene;
			public object args;
		}

		/* 
		 * Adds scene transitions on click to a list of buttons with 'Navigation' tags in the current scene.
		 * The name of each button and corresponding scene transition are passed in actions.
		 */
		public static void SetButtonTransitions(List<ActionArg> actions)
		{
			GameObject[] objects = GameObject.FindGameObjectsWithTag("Navigation");

			foreach (var obj in objects)
			{
				for (int i = 0; i < actions.Count; i++)
				{
					if (actions[i].name == obj.name)
					{
						SetButtonTransition(obj, actions[i].toScene, actions[i].args);
						actions.RemoveAt(i);
					}
				}
			}

			if (actions.Count > 0)
			{
				foreach (var action in actions)
					Debug.Log("Button '" + action.name + "' not found in current scene.");
			}
		}

		public static void SetButtonTransition(GameObject button, LevelManager.LevelState toScene, object args)
		{
			UnityAction action = () => { LevelManager.instance.TransitionLevel(toScene, args); };
			button.GetComponent<Button>().onClick.AddListener(action);
		}
	}
}
