﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour
{
	[SerializeField]
	Ball attachedBall = null;

	internal float maxSpeed = 100;
	public float velocity { get; private set; }

	// Use this for initialization
	void Start()
	{
		if (attachedBall != null)
			AttachBall(attachedBall);
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
		{
			velocity = Input.GetKey(KeyCode.D) ? maxSpeed : -maxSpeed;
			UpdatePosition();
		}

		if (Input.GetKeyDown(KeyCode.Space) && attachedBall != null)
		{
			LaunchBall();
		}

		velocity = 0;
	}

	void AttachBall(Ball ball)
	{
		Destroy(ball.gameObject.GetComponent<Rigidbody2D>());
		attachedBall = ball;
	}

	void LaunchBall()
	{
		// Creates rigidbody for ball.
		// TODO: Use kinematic rigidbody to start.
		var body = attachedBall.gameObject.AddComponent<Rigidbody2D>();
		body.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
		body.freezeRotation = true;
		body.gravityScale = 0;

		// Calculates ball velocity at launch.
		float ballSpeed = 100;
		float vxMax = ballSpeed * (Mathf.Sqrt(3) / 2); // Ball launches at minimum angle of 30 degrees from paddle.
		float vx = Mathf.Lerp(0, vxMax, Mathf.Abs(velocity) / maxSpeed) * Mathf.Sign(velocity);
		float vy = Mathf.Sqrt(ballSpeed * ballSpeed - vx * vx);
		body.AddForce(new Vector2(vx, vy), ForceMode2D.Impulse);

		attachedBall = null;
	}

	void UpdatePosition()
	{
		var disp = velocity * Time.deltaTime;
		var pos = transform.position.x + disp;

		// Calculates offsets of paddle from edge of screen.
		// TODO: Replace position adjustment logic with kinematic trigger with static screen edge colliders.
		Vector3[] corners = new Vector3[4];
		GetComponentInParent<RectTransform>().GetWorldCorners(corners);
		var sprite = GetComponent<SpriteRenderer>().sprite;
		float spriteOffset = (sprite.rect.xMax - sprite.rect.x) / sprite.pixelsPerUnit;
		float offset = transform.right.x * (GetWorldScale(transform).x / 2) * spriteOffset;
		float rightDiff = (pos + offset) - corners[2].x;
		float leftDiff = (pos - offset) - corners[0].x;

		// Adjusts final position to be within screen bounds.
		// TODO: Interpolate velocity to average speed from previous to new distance to match kinematic movement.
		if (rightDiff > 0)
			pos -= rightDiff;
		else if (leftDiff < 0)
			pos -= leftDiff;

		// Moves paddle and attached ball.
		var body = gameObject.GetComponent<Rigidbody2D>();
		body.MovePosition(new Vector2(pos, transform.position.y));
		if (attachedBall != null)
			attachedBall.transform.Translate(pos - attachedBall.transform.position.x, 0, 0);
	}

	Vector3 GetWorldScale(Transform tf)
	{
		Vector3 worldScale = tf.localScale;
		tf = tf.parent;

		while (tf != null)
		{
			worldScale.Scale(tf.localScale);
			tf = tf.parent;
		}

		return worldScale;
	}
}
