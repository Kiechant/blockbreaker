﻿using System;
using System.Collections.Generic;

namespace BlockBreaker
{
	public class MiscUtility
	{
		public class Comparer<T> : IComparer<T>
		{
			readonly Comparison<T> comparison;

			public Comparer(Func<T, T, int> compare)
			{
				if (compare == null)
				{
					throw new ArgumentNullException();
				}
				comparison = new Comparison<T>(compare);
			}

			public int Compare(T x, T y)
			{
				return comparison(x, y);
			}
		}
	}
}
