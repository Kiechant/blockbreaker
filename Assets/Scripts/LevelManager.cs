﻿using BlockBreaker;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	public static LevelManager instance { get; private set; }
	public static int TotalGameLevels = 3;

	public enum LevelState { Start, Game, Win, Lose, Pause }
	public delegate void TransitionDelegate(object args);
	Graph<LevelState, TransitionDelegate> levelGraph = new Graph<LevelState, TransitionDelegate>();

	[SerializeField] public LevelState currentLevel; // TODO: Make readonly.
	public LevelState previousLevel { get; private set; }
	public object transitionArgs { get; private set; }

	void Awake()
	{
		if (instance != null)
		{
			DestroyObject(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);

			previousLevel = currentLevel;

			// Adds level states.
			levelGraph.AddNode(LevelState.Start);
			levelGraph.AddNode(LevelState.Game);
			levelGraph.AddNode(LevelState.Win);
			levelGraph.AddNode(LevelState.Lose);
			levelGraph.AddNode(LevelState.Pause);

			// Adds level transitions.
			levelGraph.AddEdge(LevelState.Start, LevelState.Game, ToGame);
			levelGraph.AddEdge(LevelState.Game, LevelState.Start, ToStart);
			levelGraph.AddEdge(LevelState.Game, LevelState.Win, ToPost);
			levelGraph.AddEdge(LevelState.Game, LevelState.Lose, ToPost);
			levelGraph.AddEdge(LevelState.Game, LevelState.Pause, PauseGame);
			levelGraph.AddEdge(LevelState.Win, LevelState.Start, ToStart);
			levelGraph.AddEdge(LevelState.Win, LevelState.Game, ToGame);
			levelGraph.AddEdge(LevelState.Lose, LevelState.Start, ToStart);
			levelGraph.AddEdge(LevelState.Lose, LevelState.Game, ToGame);
			levelGraph.AddEdge(LevelState.Pause, LevelState.Start, ToStart);
			levelGraph.AddEdge(LevelState.Pause, LevelState.Game, ResumeGame);
		}
	}

	public void TransitionLevel(string level)
	{
		LevelState levelState;

		try
		{
			levelState = (LevelState) Enum.Parse(typeof(LevelState), level);
		}
		catch (ArgumentException)
		{
			Debug.Log("'" + level + "' is not a valid level state.");
			return;
		}

		TransitionLevel(levelState, null);
	}

	public bool TransitionLevel(LevelState level, object args)
	{
		var fn = levelGraph.FindNode(currentLevel);
		int index = fn.FindAdjacentIndex(level);

		if (index != -1)
		{
			previousLevel = currentLevel;
			currentLevel = level;
			transitionArgs = args;

			fn.GetTransition(index)(args);
			return true;
		}

		Debug.Log(string.Format("No transition from current level '{0}' to next level '{1}'.", currentLevel, level));
		return false;
	} 

	public void LoadLevel(string name)
    {
		Debug.Log("New Level load: " + name);
		SceneManager.LoadScene(name);
	}

	public void QuitRequest()
    {
		Debug.Log("Quit requested");
		Application.Quit();
	}

	void ToStart(object args)
	{
		SceneManager.LoadScene("Start Menu");
	}

	public struct ToGameArgs
	{
		public int levelNumber;
	}

	void ToGame(object args)
	{
		SceneManager.LoadScene("Game Screen");
	}

	public struct ToPostArgs
	{
		public int prevLevel;
	}

	void ToPost(object args)
	{
		SceneManager.LoadScene("Post Screen");
	}
	
	void PauseGame(object args)
	{
		// TODO: Add pausing logic here.
	}

	void ResumeGame(object args)
	{
		// TODO: Add resuming logic here.
	}
}
