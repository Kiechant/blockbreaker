﻿using LM = LevelManager;
using Nav = BlockBreaker.Navigation;
using System.Collections.Generic;
using UnityEngine;

public class StartManager : MonoBehaviour
{
	void Start()
	{
		// Configures UI navigation.
		var actions = new List<Nav.ActionArg>();
		actions.Add(new Nav.ActionArg("Play Game", LM.LevelState.Game, new LM.ToGameArgs { levelNumber = 1 }));
		Nav.SetButtonTransitions(actions);
	}
}
