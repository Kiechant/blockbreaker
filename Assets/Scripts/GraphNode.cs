﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BlockBreaker
{
	public class GraphNode<T, U>
	{
		public T item;
		List<GraphNode<T, U>> adjacent = new List<GraphNode<T, U>>();
		List<U> transition = new List<U>();

		public int AdjacentCount
		{
			get { return adjacent.Count; }
		}

		public GraphNode() {}

		public GraphNode(T item)
		{
			this.item = item;
		}

		public void AddAdjacent(GraphNode<T, U> node, U transition)
		{
			adjacent.Add(node);
			this.transition.Add(transition);
		}

		public bool RemoveAdjacent(GraphNode<T, U> node)
		{
			int index = adjacent.IndexOf(node);

			if (index != -1)
			{
				adjacent.RemoveAt(index);
				transition.RemoveAt(index);
				return true;
			}

			return false;
		}

		public GraphNode<T, U> GetAdjacent(int index)
		{
			return adjacent[index];
		}

		public GraphNode<T, U> FindAdjacent(T item)
		{
			int index = FindAdjacentIndex(item);
			if (index != -1)
				return adjacent[index];
			return null;
		}

		public int FindAdjacentIndex(T item)
		{
			return adjacent.FindIndex(0, adjacent.Count, (GraphNode<T, U> adj) => { return adj.item.Equals(item); });
		}

		public U GetTransition(int index)
		{
			return transition[index];
		}
	}
}
