﻿using LM = LevelManager;
using Nav = BlockBreaker.Navigation;
using UnityEngine;
using UnityEngine.UI;

public class PostManager : MonoBehaviour
{
	public GameObject titleLabel;
	public GameObject subtitleLabel;
	public GameObject primaryButton;
	public GameObject secondaryButton;
	public GameObject homeButton;

	int prevLevel;

	void Start()
	{
		prevLevel = ((LM.ToPostArgs) LM.instance.transitionArgs).prevLevel;

		Nav.SetButtonTransition(homeButton, LM.LevelState.Start, null);

		if (LM.instance.currentLevel == LM.LevelState.Win)
			GenerateWin();
		else if (LM.instance.currentLevel == LM.LevelState.Lose)
			GenerateLoss();
		else throw new System.Exception("Invalid level state '" + LM.instance.currentLevel + "' at post screen.");
	}

	void GenerateWin()
	{
		GenerateWinTitles();
		GenerateWinButtons();
	}

	void GenerateWinTitles()
	{
		string[] winTitle = {
			" LEVEL\nSMASHED",
			"LEVEL \n DESTROYED!",
			"CHALLENGE \nCRUSHED!",
			" DESTINY\nMET"
		};

		string[] winSubtitle = {
			"But the game has just begun",
			"Careful the next doesn't destroy YOU",
			"And your soul with it",
			"Deep fulfilment be yours"
		};

		float[] winChance = {
			0.5f, 0.3f, 0.15f, 0.05f
		};

		float p = Random.value;
		float wint = winChance[0];
		int wini = 0;

		while (p > wint + Mathf.Epsilon)
		{
			wini++;
			wint += winChance[wini];
		}

		titleLabel.GetComponent<Text>().text = winTitle[wini];
		subtitleLabel.GetComponent<Text>().text = winSubtitle[wini];
	}

	void GenerateWinButtons()
	{
		int nextLevel = prevLevel + 1;
		if (nextLevel <= LM.TotalGameLevels)
		{
			Nav.SetButtonTransition(primaryButton, LM.LevelState.Game, new LM.ToGameArgs { levelNumber = prevLevel + 1 });
			primaryButton.GetComponent<Text>().text = "Next Level";

			Nav.SetButtonTransition(secondaryButton, LM.LevelState.Game, new LM.ToGameArgs { levelNumber = prevLevel });
			secondaryButton.GetComponent<Text>().text = "Replay";
			secondaryButton.SetActive(true);
		}
		else
		{
			Nav.SetButtonTransition(primaryButton, LM.LevelState.Game, new LM.ToGameArgs { levelNumber = prevLevel });
			primaryButton.GetComponent<Text>().text = "Replay";
		}
	}

	void GenerateLoss()
	{
		GenerateLossTitles();
		GenerateLossButtons();
	}

	// TODO: Reuse code from GenerateWinText.
	void GenerateLossTitles()
	{
		string[] lossTitle = {
			" GAME\nOVER",
			"BATTLE \n LOST!",
			"SOUL \nCRUSHED!",
			" N00B\nPWNED"
		};

		string[] lossSubtitle = {
			"Again?",
			"But maybe not the war",
			"Is that a tear?",
			"Git gud scrub!"
		};

		float[] lossChance = {
			0.5f, 0.3f, 0.15f, 0.05f
		};

		float p = Random.value;
		float wint = lossChance[0];
		int wini = 0;

		while (p > wint + Mathf.Epsilon)
		{
			wini++;
			wint += lossChance[wini];
		}

		titleLabel.GetComponent<Text>().text = lossTitle[wini];
		subtitleLabel.GetComponent<Text>().text = lossSubtitle[wini];
	}

	void GenerateLossButtons()
	{
		Nav.SetButtonTransition(primaryButton, LM.LevelState.Game, new LM.ToGameArgs { levelNumber = prevLevel });
		primaryButton.GetComponent<Text>().text = "Retry";
	}
}
