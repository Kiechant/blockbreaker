﻿using LM = LevelManager;
using Nav = BlockBreaker.Navigation;
using Misc = BlockBreaker.MiscUtility;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
	const int XIntervals = 32;
	const int YIntervals = 14;

	[SerializeField] GameObject blockArea;
	[SerializeField] GameObject gameScreen;
	Dictionary<char, GameObject> blocksByCode = new Dictionary<char, GameObject>();

	int currLevel;
	List<GameObject> blocks = new List<GameObject>();

	void Start()
	{
		// Determines current level from transition.
		if (LM.instance.previousLevel != LM.LevelState.Game)
			currLevel = ((LM.ToGameArgs) LM.instance.transitionArgs).levelNumber;
		else currLevel = 1;

		// Configures UI navigation.
		var actions = new List<Nav.ActionArg>();
		actions.Add(new Nav.ActionArg("Exit Level", LM.LevelState.Start, null));
		actions.Add(new Nav.ActionArg("Win Game", LM.LevelState.Win, new LM.ToPostArgs { prevLevel = currLevel }));
		Nav.SetButtonTransitions(actions);

		// Loads level and resources.
		LoadBlocks();
		LoadLevel(currLevel);
	}

	/*
	 * Loads block prefabs from the Resources folder into the blocks array as game objects.
	 */
	void LoadBlocks()
	{
		GameObject[] blockLoad = Resources.LoadAll<GameObject>("Blocks");

		var blockNamesToCode = new Dictionary<string, char>
		{
			{ "Block Single", '0' },
			{ "Block Double", '1' },
			{ "Block Triple", '2' }
		};

		foreach (var block in blockLoad)
		{
			char blockCode;
			if (blockNamesToCode.TryGetValue(block.name, out blockCode))
			{
				blocksByCode[blockCode] = block;
			}
			else
			{
				string errMsg = string.Format("Attempting to load unrecognised block name '{0}'.", block.name);
				throw new System.ArgumentException(errMsg);
			}
		}
	}

	/*
	 * Loads the level specified by number and populates the game area with blocks.
	 */
	void LoadLevel(int number)
	{
		// Loads level text file.
		var level = Resources.Load<TextAsset>("Levels/Level_" + number.ToString("00"));

		// Reads text grid as a string stream and populates the game area.
		bool[,] occupied = new bool[XIntervals, YIntervals];

		using (var sr = new StringReader(level.text))
		{
			for (int y = 0; y < YIntervals; y++)
			{
				for (int x = 0; x < XIntervals; x++)
				{
					char code = (char)sr.Read();
					while (char.IsWhiteSpace(code))
						code = (char)sr.Read();

					LoadBlock(code, x, y, ref occupied);
				}
			}
		}

		blocks.Sort(CompareGameObjects);
	}

	void LoadBlock(char code, int x, int y, ref bool[,] occupied)
	{
		if (code != '.' && code != '\n')
		{
			if (code == '+')
			{
				if (!occupied[x,y])
					Debug.Log(string.Format("Character '+' used for unoccupied space at position ({0}, {1}).", x, y));
			}
			else if (blocksByCode.ContainsKey(code))
			{
				var blockGO = Instantiate(blocksByCode[code]);
				blocks.Add(blockGO);

				var block = blockGO.GetComponent<Block>();
				block.gameplayManager = this;

				int xMax = x + block.width;
				int yMax = y + block.height;

				if (xMax > XIntervals || yMax > YIntervals)
					Debug.Log(string.Format("Block '{0}' at ({1}, {2}) exceeds bounds of block area.", code, x, y));

				xMax = Mathf.Min(xMax, XIntervals);
				yMax = Mathf.Min(yMax, YIntervals);

				for (int j = y; j < yMax; j++)
					for (int i = x; i < xMax; i++)
						occupied[i,j] = true;

				// Calculates position for block in game screen.
				Rect baRect = ((RectTransform) blockArea.transform).rect;
				Vector2 blockCentre = new Vector2(x + block.width / 2, y + block.height / 2);
				Vector2 rectDisp = new Vector2(blockCentre.x * (baRect.width / XIntervals), blockCentre.y * (baRect.height / YIntervals));
				Vector2 rectPos = new Vector2(baRect.xMin + rectDisp.x, baRect.yMax - rectDisp.y);

				// Sets position of block inside the game screen.
				blockGO.transform.parent = blockArea.transform;
				blockGO.transform.localScale = new Vector3(1, 1, 1);
				blockGO.transform.localPosition = new Vector3(rectPos.x, rectPos.y, 0);
			}
			else
			{
				string errMsg = string.Format("Attempting to instantiate block with unrecognised code '{0}' at position ({1}, {2}).", code, x, y);
				throw new System.ArgumentException(errMsg);
			}
		}
	}

	static int CompareGameObjects(GameObject x, GameObject y)
	{
		if (x.GetInstanceID() < y.GetInstanceID()) return -1;
		if (x.GetInstanceID() > y.GetInstanceID()) return 1;
		return 0;
	}

	public bool DestroyBlock(GameObject block)
	{
		int index = blocks.BinarySearch(block, new Misc.Comparer<GameObject>(CompareGameObjects));

		if (index != -1)
		{
			DestroyObject(block);
			blocks.RemoveAt(index);

			if (blocks.Count == 0)
			{
				LM.instance.TransitionLevel(LM.LevelState.Win, new LM.ToPostArgs { prevLevel = currLevel });
			}

			return true;
		}
		return false;
	}

	public void GameOver()
	{
		LM.instance.TransitionLevel(LM.LevelState.Lose, new LM.ToPostArgs{ prevLevel = currLevel });
	}
}
