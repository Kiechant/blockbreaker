﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour
{
	public GameplayManager gameplayManager;

	public int height;
	public int width;
	public int health;

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.GetComponent<Ball>() != null)
		{
			health--;
			if (health == 0)
			{
				gameplayManager.DestroyBlock(gameObject);
			}
		}
	}
}
