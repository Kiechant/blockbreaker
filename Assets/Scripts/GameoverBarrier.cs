﻿using UnityEngine;
using System.Collections;

public class GameoverBarrier : MonoBehaviour
{
	public GameplayManager gameplayManager;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.GetComponent<Ball>() != null)
			gameplayManager.GameOver();
	}
}
