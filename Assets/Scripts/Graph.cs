﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BlockBreaker
{
	public class Graph<T, U>
	{
		List<GraphNode<T, U>> nodes = new List<GraphNode<T, U>>();

		public int NodeCount
		{
			get { return nodes.Count; }
		}

		public void AddNode(T item)
		{
			nodes.Add(new GraphNode<T, U>(item));
		}

		public bool RemoveNode(T item)
		{
			int index = FindNodeIndex(item);

			if (index != -1)
			{
				foreach (var node in nodes)
					node.RemoveAdjacent(nodes[index]);
				nodes.RemoveAt(index);

				return true;
			}

			return false;
		}

		public GraphNode<T, U> GetNode(int index)
		{
			return nodes[index];
		}

		public GraphNode<T, U> FindNode(T item)
		{
			int index = FindNodeIndex(item);
			if (index != -1)
				return nodes[index];
			return null;
		}

		public int FindNodeIndex(T item)
		{
			return nodes.FindIndex(0, nodes.Count, (GraphNode<T, U> node) => { return node.item.Equals(item); });
		}

		public bool AddEdge(T first, T second, U transition, bool bidirectional = false)
		{
			int fi = FindNodeIndex(first);
			int si = FindNodeIndex(second);

			if (fi != -1 && si != -1)
			{
				nodes[fi].AddAdjacent(nodes[si], transition);
				if (bidirectional)
					nodes[si].AddAdjacent(nodes[fi], transition);
				
				return true;
			}

			return false;
		}

		public bool RemoveEdge(T first, T second, bool bidirectional = false)
		{
			int fi = FindNodeIndex(first);
			int si = FindNodeIndex(second);

			if (fi != -1 && si != -1)
			{
				nodes[fi].RemoveAdjacent(nodes[si]);
				if (bidirectional)
					nodes[si].RemoveAdjacent(nodes[fi]);

				return true;
			}

			return false;
		}

		public bool HasEdge(T first, T second)
		{
			return (FindNodeIndex(first) != -1 && FindNodeIndex(second) != -1);
		}

		public U FindEdgeTransition(T first, T second)
		{
			GraphNode<T, U> fn = FindNode(first);
			int index = fn.FindAdjacentIndex(second);
			if (index != -1)
				return fn.GetTransition(index);
			return default(U);
		}
	}
}